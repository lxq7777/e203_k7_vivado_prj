set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]

#####               create clock              #####

set_property -dict {PACKAGE_PIN F17 IOSTANDARD LVCMOS33} [get_ports CLK50MHZ]
create_clock -period 20.000 -name sys_clk_pin -waveform {0.000 10.000} -add [get_ports CLK50MHZ]


#####            rst define           #####

set_property PACKAGE_PIN C24 [get_ports sys_rst_n]
set_property IOSTANDARD LVCMOS33 [get_ports sys_rst_n]

#####                spi0 define               #####
set_property PACKAGE_PIN C23 [get_ports qspi0_cs]
set_property PACKAGE_PIN A22 [get_ports {qspi0_dq[3]}]
set_property PACKAGE_PIN B22 [get_ports {qspi0_dq[2]}]
set_property PACKAGE_PIN A25 [get_ports {qspi0_dq[1]}]
set_property PACKAGE_PIN B24 [get_ports {qspi0_dq[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports qspi0_cs]
set_property IOSTANDARD LVCMOS33 [get_ports {qspi0_dq[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {qspi0_dq[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {qspi0_dq[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {qspi0_dq[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports rx]
set_property IOSTANDARD LVCMOS33 [get_ports tx]
set_property PACKAGE_PIN L23 [get_ports tx]
set_property PACKAGE_PIN K21 [get_ports rx]

#####         SPI Configurate Setting        #######
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 50 [current_design]

