module m256_div_feq(
	input	wire	clk8_388m,
	output	wire	clk32_768k
);

reg [7:0] counter;
reg	clk32_768k_r=1'b0;

always @(posedge clk8_388m) begin
    if (counter == 8'd127) begin
      counter <= 0;
      clk32_768k_r <= ~clk32_768k_r; // Toggle output clock every 256 cycles
    end else begin
      counter <= counter + 1;
    end
end

assign clk32_768k = clk32_768k_r;
	
endmodule